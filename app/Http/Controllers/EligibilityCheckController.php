<?php
/**
 * @package    API
 * @author     Yudhistira Eka Putra
 */

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EligibilityCheckController extends Controller
{
    /**
     * Create a new object.
     *
     * @return void
     */
    public function __construct(){
        $this->client = new \GuzzleHttp\Client(['base_uri' => env('API_URL')]);;
    }

    /**
     * Handle check indihome number request.
     *
     * @param  object $request
     * @return json
     */
    public function check(Request $request)
    {
        $validator = Validator::make($request->input(), [
            'indihome_number' => 'bail|required|string',
        ]);

        if ($validator->fails()) {
            return $validator->errors();
        }

        $data = [
            'indihome_number' => $request->input('indihome_number'),
        ];

        $res = $this->client->request('POST', '/rest/telkom/bie/addon/vidCall/API/eligibilityCheck', [
            'form_params' => $data
        ]);

        return $res->getBody();
    }
}
